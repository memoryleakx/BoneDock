﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace io.github.memoryleakx.BoneDockLib.Class
{
    public class ProcessMonitor
    {
        #region Static Fields and Constants

        public static readonly ulong WS_BORDER = 0x00800000L;
        public static readonly ulong WS_VISIBLE = 0x10000000L;
        public static readonly ulong DESIRED_WS = WS_BORDER | WS_VISIBLE;
        public static readonly int GWL_STYLE = -16;

        #endregion

        #region Events and Delegates

        public delegate bool EnumWindowsCallback(IntPtr hwnd, int lParam);

        #endregion

        #region  Methods

        [DllImport("user32.dll")]
        private static extern int EnumWindows(EnumWindowsCallback lpEnumFunc, int lParam);

        /// <summary>
        ///     Gets all windows.
        /// </summary>
        /// <returns></returns>
        public static List<IntPtr> GetAllWindows()
        {
            var windows = new List<IntPtr>();
            EnumWindows(delegate(IntPtr hwnd, int lParam)
            {
                if ((GetWindowLongA(hwnd, GWL_STYLE) & DESIRED_WS) == DESIRED_WS)
                {
                    windows.Add(hwnd);
                }
                return true;
            }, 0);

            return windows;
        }

        /// <summary>
        ///     Gets the window long a.
        /// </summary>
        /// <param name="hWnd">The h WND.</param>
        /// <param name="nIndex">Index of the n.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        private static extern ulong GetWindowLongA(IntPtr hWnd, int nIndex);

        /// <summary>
        ///     Gets the window text.
        /// </summary>
        /// <param name="hWnd">The h WND.</param>
        /// <param name="lpString">The lp string.</param>
        /// <param name="nMaxCount">The n maximum count.</param>
        [DllImport("user32.dll")]
        public static extern void GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        /// <summary>
        ///     Gets the window thread process identifier.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern int GetWindowThreadProcessId(IntPtr hwnd, out int id);

        #endregion
    }
}