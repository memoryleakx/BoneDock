﻿// <copyright file="DockletLoader.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using io.github.memoryleakx.BoneDockLib.Helper;
using io.github.memoryleakx.BoneDockLib.Interfaces;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Class
{
    public class DockletLoader : IDisposable
    {
        #region Fields

        private readonly string m_dockletDir;
        private readonly FileSystemWatcher m_fileSystemWatcher;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DockletLoader" /> class.
        /// </summary>
        public DockletLoader()
        {
            Docklets = new IDocklet[0];
            string programmData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            m_dockletDir = programmData + Path.DirectorySeparatorChar + "BoneDock" + Path.DirectorySeparatorChar +
                           "Docklets" + Path.DirectorySeparatorChar;

            if (FileHelper.HasReadWriteAccessToFolder(programmData))
            {
                Directory.CreateDirectory(m_dockletDir);
            }

            if (FileHelper.HasReadAccessToFolder(m_dockletDir))
            {
                m_fileSystemWatcher = new FileSystemWatcher(m_dockletDir);
                m_fileSystemWatcher.Created += FileSystemWatcher_Handler;
                m_fileSystemWatcher.Renamed += FileSystemWatcher_Handler;
                m_fileSystemWatcher.Changed += FileSystemWatcher_Handler;
                m_fileSystemWatcher.Deleted += FileSystemWatcher_Handler;
            }
        }

        #endregion

        #region Events and Delegates

        /// <summary>
        ///     Occurs when [docklets received].
        /// </summary>
        public event DockletsReceivedDelegate DockletsReceived;

        public delegate void DockletsReceivedDelegate(IDocklet[] docklets);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the docklets.
        /// </summary>
        /// <value>
        ///     The docklets.
        /// </value>
        private IEnumerable<IDocklet> Docklets { get; set; }

        #endregion

        #region  Methods

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (null != m_fileSystemWatcher)
            {
                m_fileSystemWatcher.Created -= FileSystemWatcher_Handler;
                m_fileSystemWatcher.Renamed -= FileSystemWatcher_Handler;
                m_fileSystemWatcher.Changed -= FileSystemWatcher_Handler;
                m_fileSystemWatcher.Deleted -= FileSystemWatcher_Handler;
                m_fileSystemWatcher.Dispose();
            }
            Docklets = null;
        }

        /// <summary>
        ///     Handles the Handler event of the FileSystemWatcher control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FileSystemEventArgs" /> instance containing the event data.</param>
        private void FileSystemWatcher_Handler(object sender, FileSystemEventArgs e)
        {
            ReloadDocklets();
        }

        /// <summary>
        ///     Loads this instance.
        /// </summary>
        public void Load()
        {
            ReloadDocklets();
        }

        /// <summary>
        ///     Called when [docklets received].
        /// </summary>
        /// <param name="docklets">The docklets.</param>
        protected virtual void OnDockletsReceived(IDocklet[] docklets)
        {
            DockletsReceived?.Invoke(docklets);
        }

        private void ReloadDocklets()
        {
            if (!Directory.Exists(m_dockletDir))
            {
                return;
            }

            if (m_dockletDir != null)
            {
                string[] dockletFiles = Directory.GetFiles(m_dockletDir, "*.dll");
                Docklets = (
                    // From each file in the files.
                    from file in dockletFiles
                    // Load the assembly.
                    let asm = Assembly.LoadFile(file)
                    // For every type in the assembly that is visible outside of
                    // the assembly.
                    from type in asm.GetExportedTypes()
                    // Where the type implements the interface.
                    where typeof(IDocklet).IsAssignableFrom(type)
                    // Create the instance.
                    select (IDocklet) Activator.CreateInstance(type)
                    // Materialize to an array.
                    ).ToArray();
            }
            OnDockletsReceived(Docklets.ToArray());
        }

        #endregion
    }
}