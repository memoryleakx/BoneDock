﻿// <copyright file="ProcessController.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-07-12</date>

#region

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using io.github.memoryleakx.BoneDockLib.Enums;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Class
{
    public class ProcessController : IDisposable
    {
        #region Fields

        private readonly CancellationToken m_cancellationToken;
        private readonly CancellationTokenSource m_cancellationTokenSource;

        private readonly List<ProcessWindowInfo> m_processWindowInfo;
        private Point m_cursorPos;
        private readonly object m_lock = new object();
        private PositionEnum m_position;
        private double m_screenHeight;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ProcessController" /> class.
        /// </summary>
        public ProcessController()
        {
            m_cursorPos = new Point();
            m_cancellationTokenSource = new CancellationTokenSource();
            m_cancellationToken = m_cancellationTokenSource.Token;
            m_processWindowInfo = new List<ProcessWindowInfo>();
            Polling();
        }

        #endregion

        #region Events and Delegates

        /// <summary>
        ///     Occurs when [window intersection event].
        /// </summary>
        public event WindowIntersectionEventDelegate WindowIntersectionEvent;

        /// <param name="intersect">if set to <c>true</c> [intersect].</param>
        public delegate void WindowIntersectionEventDelegate(bool intersect);

        #endregion

        #region  Methods

        /// <summary>
        ///     Checks the intersection.
        /// </summary>
        /// <param name="processWin">The process win.</param>
        /// <returns></returns>
        public bool CheckIntersection(ProcessWindowInfo processWin)
        {
            var intersect = false;
            var processRect = new NativeInfos.Rect();
            NativeInfos.GetWindowRect(processWin.MainWindowHandle, ref processRect);

            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(delegate
                {
                    if (Application.Current == null)
                    {
                        return intersect;
                    }

                    Rectangle docRect = GetDockRectangle();
                    Rectangle appRect = GetApplicationRectangle(processRect);


                    // Check Intersect
                    intersect = appRect.IntersectsWith(docRect);

                    // Get Global Cursor Position
                    NativeInfos.GetCursorPos(ref m_cursorPos);

                    switch (m_position)
                    {
                        case PositionEnum.Top:
                            if (m_cursorPos.Y < 70.0)
                            {
                                intersect = false;
                            }
                            break;
                        case PositionEnum.Bottom:
                            if (m_cursorPos.Y > m_screenHeight - 70.0)
                            {
                                intersect = false;
                            }
                            break;
                    }
                    return intersect;
                }), null);

            return intersect;
        }


        /// <summary>
        ///     Checks the positions.
        /// </summary>
        private void CheckPositions()
        {
            FetchAllProcesses();
            var intersects = false;
            lock (m_processWindowInfo)
            {
                if (m_processWindowInfo.Count == 0)
                {
                    return;
                }

                lock (m_lock)
                {
                    foreach (ProcessWindowInfo p in m_processWindowInfo)
                    {
                        intersects = CheckIntersection(p);
                        if (!intersects)
                        {
                            continue;
                        }
                        Debug.WriteLine(p.MainWindowHandle);
                        break;
                    }
                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                        new DispatcherOperationCallback(delegate
                        {
                            OnWindowIntersectionEvent(intersects);
                            return null;
                        }), null);
                }
            }
        }


        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            m_cancellationTokenSource?.Cancel();
            m_processWindowInfo?.Clear();
        }

        /// <summary>
        ///     Fetches all processes.
        /// </summary>
        private void FetchAllProcesses()
        {
            lock (m_processWindowInfo)
            {
                m_processWindowInfo.Clear();

                IEnumerable<IntPtr> openWindowProcesses = ProcessMonitor.GetAllWindows();

                foreach (IntPtr windowId in openWindowProcesses)
                {
                    var processRect = new NativeInfos.Rect();
                    NativeInfos.GetWindowRect(windowId, ref processRect);
                    var pwi = new ProcessWindowInfo {MainWindowHandle = windowId, Position = processRect};

                    if (processRect.Bottom < 0)
                    {
                        continue;
                    }

                    m_processWindowInfo.Add(pwi);
                }
            }
        }

        /// <summary>
        ///     Gets the application rectangle.
        /// </summary>
        /// <param name="processRect">The process rect.</param>
        /// <returns></returns>
        private static Rectangle GetApplicationRectangle(NativeInfos.Rect processRect)
        {
            // Get other App Size and Location
            var appWinSize = new Size
            {
                Height = processRect.Bottom - processRect.Top,
                Width = processRect.Right - processRect.Left
            };
            var appWinLocation = new Point {X = processRect.Left, Y = processRect.Top};
            var appRect = new Rectangle(appWinLocation, appWinSize);
            return appRect;
        }

        /// <summary>
        ///     Gets the dock rectangle.
        /// </summary>
        /// <returns></returns>
        private static Rectangle GetDockRectangle()
        {
            // Get Dock Size and Location
            Window dock = Application.Current.MainWindow;
            var dockWinSize = new Size {Height = (int) Math.Round(dock.Height), Width = (int) Math.Round(dock.Width)};
            var dockWinLocation = new Point {X = (int) Math.Round(dock.Left), Y = (int) Math.Round(dock.Top)};
            var docRect = new Rectangle(dockWinLocation, dockWinSize);
            return docRect;
        }

        /// <summary>
        ///     Determines whether the specified main window handle is active.
        /// </summary>
        /// <param name="mainWindowHandle">The main window handle.</param>
        /// <returns></returns>
        private static bool IsActive(IntPtr mainWindowHandle)
        {
            return NativeInfos.GetForegroundWindow() == mainWindowHandle;
        }

        /// <summary>
        ///     Called when [window intersection event].
        /// </summary>
        /// <param name="intersect">if set to <c>true</c> [intersect].</param>
        protected virtual void OnWindowIntersectionEvent(bool intersect)
        {
            WindowIntersectionEvent?.Invoke(intersect);
        }

        /// <summary>
        ///     Pollings this instance.
        /// </summary>
        private void Polling()
        {
            const int delay = 500;
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    if (m_cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }
                    CheckPositions();
                    Thread.Sleep(delay);
                }
            }, m_cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        /// <summary>
        ///     Sets the dock position.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="screenHeight">Height of the screen.</param>
        public void SetDockPosition(PositionEnum position, double screenHeight)
        {
            m_position = position;
            m_screenHeight = screenHeight;
        }

        #endregion
    }
}