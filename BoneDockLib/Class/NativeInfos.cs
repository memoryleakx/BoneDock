﻿// <copyright file="NativeInfos.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System;
using System.Drawing;
using System.Runtime.InteropServices;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Class
{
    public class NativeInfos
    {
        #region Static Fields and Constants

        public const int SW_RESTORE = 9;
        public const int SW_SHOW = 5;

        #endregion

        #region  Methods

        /// <summary>
        ///     Gets the cursor position.
        /// </summary>
        /// <param name="lpPoint">The lp point.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        /// <summary>
        ///     Gets the window rect.
        /// </summary>
        /// <param name="hwnd">The HWND.</param>
        /// <param name="rectangle">The rectangle.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);


        /// <summary>
        ///     Shows the window.
        /// </summary>
        /// <param name="hWnd">The h WND.</param>
        /// <param name="nCmdShow">The n command show.</param>
        /// <returns></returns>
        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        #endregion

        public struct Rect
        {
            /// <summary>
            ///     Gets or sets the left.
            /// </summary>
            /// <value>
            ///     The left.
            /// </value>
            public int Left { get; set; }

            /// <summary>
            ///     Gets or sets the top.
            /// </summary>
            /// <value>
            ///     The top.
            /// </value>
            public int Top { get; set; }

            /// <summary>
            ///     Gets or sets the right.
            /// </summary>
            /// <value>
            ///     The right.
            /// </value>
            public int Right { get; set; }

            /// <summary>
            ///     Gets or sets the bottom.
            /// </summary>
            /// <value>
            ///     The bottom.
            /// </value>
            public int Bottom { get; set; }
        }
    }
}