﻿// <copyright file="Launcher.xaml.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-07-12</date>

#region

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Threading;
using io.github.memoryleakx.BoneDockLib.Class;
using io.github.memoryleakx.BoneDockLib.Enums;
using io.github.memoryleakx.BoneDockLib.Interfaces;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDockLib.UserControls
{
    /// <summary>
    ///     Interaction logic for Launcher.xaml
    /// </summary>
    public partial class Launcher : IDockUiControl
    {
        #region Fields

        private readonly PositionEnum m_position;

        private Process m_currentProcess;
        private string m_currentProcessName = string.Empty;
        private Border m_indicator;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Launcher" /> class.
        /// </summary>
        public Launcher(AppStarter starter, DockItem parent, PositionEnum dockPosition)
        {
            InitializeComponent();
            DockItem = parent;
            LauncherContextMenu.Source = this;
            AppStarter = starter;
            Image ico = starter?.Icon;
            if (ico == null)
            {
                return;
            }
            ico.Width = 45;
            ico.Height = 45;
            Content = ico;

            m_position = dockPosition;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the application starter.
        /// </summary>
        /// <value>
        ///     The application starter.
        /// </value>
        public AppStarter AppStarter { get; }

        /// <summary>
        ///     Gets the context menu element that should appear whenever the context menu is requested through user interface (UI)
        ///     from within this element.
        /// </summary>
        public new ContextMenu ContextMenu => LauncherContextMenu;

        /// <summary>
        ///     Gets the dock item.
        /// </summary>
        /// <value>
        ///     The dock item.
        /// </value>
        public DockItem DockItem { get; }

        /// <summary>
        ///     Gets the UI control.
        /// </summary>
        /// <value>
        ///     The UI control.
        /// </value>
        public FrameworkElement UiControl => this;

        #endregion

        #region  Methods

        /// <summary>
        ///     Checks the process.
        /// </summary>
        private void CheckProcess()
        {
            if (IsRunning(m_currentProcessName))
            {
                Process[] processList = Process.GetProcessesByName(m_currentProcessName);
                if (processList.Length > 0)
                {
                    m_currentProcess = processList[0];
                }
            }
            else
            {
                m_currentProcess = null;
                m_currentProcessName = string.Empty;

                if (m_indicator == null)
                {
                    return;
                }
                Action action = delegate { m_indicator.Visibility = Visibility.Collapsed; };
                Dispatcher.Invoke(DispatcherPriority.Normal, action);
            }
        }

        public void Dispose()
        {
            m_currentProcess = null;
            m_currentProcessName = null;
        }

        /// <summary>
        ///     Determines whether this instance is running.
        /// </summary>
        /// <returns></returns>
        private static bool IsRunning(string processName)
        {
            return Process.GetProcessesByName(processName).Length > 0;
        }

        /// <summary>
        ///     Handles the OnClick event of the Launcher control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        /// <exception cref="NotImplementedException"></exception>
        private void Launcher_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (null == m_currentProcess)
                {
                    StartCurrentProcess();
                }
                else
                {
                    if ("explorer" == m_currentProcess.ProcessName) // always start a new File Explorer
                    {
                        StartCurrentProcess();
                        return;
                    }

                    if (Keyboard.Modifiers == ModifierKeys.Control)
                    {
                        // start Process again
                        StartCurrentProcess();
                    }
                    else
                    {
                        // bring Process Window into front
                        IntPtr processWindowHandle = m_currentProcess.MainWindowHandle;
                        NativeInfos.ShowWindow(processWindowHandle, NativeInfos.SW_SHOW);
                        NativeInfos.ShowWindow(processWindowHandle, NativeInfos.SW_RESTORE);
                        NativeInfos.SetForegroundWindow(processWindowHandle);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        /// <summary>
        ///     Handles the Exited event of the LauncherProcess control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void LauncherProcess_Exited(object sender, EventArgs e)
        {
            CheckProcess();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            m_indicator = Template.FindName("IndicatorBorder", this) as Border;

            if (null == m_indicator)
            {
                return;
            }

            switch (m_position)
            {
                case PositionEnum.Bottom:
                    m_indicator.VerticalAlignment = VerticalAlignment.Bottom;
                    m_indicator.Margin = new Thickness(0, 0, 0, -5);
                    break;

                case PositionEnum.Top:
                    m_indicator.VerticalAlignment = VerticalAlignment.Top;
                    m_indicator.Margin = new Thickness(0, -5, 0, 0);
                    break;
            }
            PreCheckProcess();
        }

        /// <summary>
        ///     Check if the Process is already running (aufter the Launcher is created)
        /// </summary>
        private void PreCheckProcess()
        {
            m_currentProcessName = Path.GetFileNameWithoutExtension(AppStarter.FileName);
            if ("explorer" == m_currentProcessName)
            {
                return;
            }
            Process[] processlist = Process.GetProcessesByName(m_currentProcessName);
            if (!processlist.Any())
            {
                return;
            }

            var eventRegistered = true;
            foreach (Process process in processlist)
            {
                try
                {
                    process.EnableRaisingEvents = true;
                }
                catch (Exception ex)
                {
                    eventRegistered = false;
                    Debug.WriteLine(ex.Message);
                }

                if (!eventRegistered)
                {
                    continue;
                }

                process.Exited += LauncherProcess_Exited;
                m_currentProcess = process;
            }

            if (!eventRegistered)
            {
                return;
            }
            m_indicator.Visibility = Visibility.Visible;
        }

        /// <summary>
        ///     Sets the dock position.
        /// </summary>
        /// <param name="position">The position.</param>
        public void SetDockPosition(PositionEnum position)
        {
        }

        /// <summary>
        ///     Starts the current process.
        /// </summary>
        private void StartCurrentProcess()
        {
            m_currentProcess = null;

            var info = new ProcessStartInfo(AppStarter.FileName)
            {
                WorkingDirectory = AppStarter.WorkDirectory,
                UseShellExecute = true
            };
            m_currentProcess = Process.Start(info);

            if (m_currentProcess == null)
            {
                return;
            }
            m_currentProcessName = m_currentProcess.ProcessName;

            if ("explorer" == m_currentProcess.ProcessName)
            {
                return;
            }

            m_currentProcess.EnableRaisingEvents = true;
            m_currentProcess.Exited += LauncherProcess_Exited;

            if (m_indicator != null)
            {
                m_indicator.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        ///     Tools the tip opened handler.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void ToolTipOpenedHandler(object sender, RoutedEventArgs e)
        {
            var toolTip = (ToolTip) sender;
            UIElement target = toolTip.PlacementTarget;
            Point adjust = target.TranslatePoint(new Point(8, 0), toolTip);
            if (adjust.Y > 0)
            {
                toolTip.Placement = PlacementMode.Top;
            }
        }

        #endregion
    }
}