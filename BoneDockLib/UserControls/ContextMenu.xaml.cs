﻿// <copyright file="ContextMenu.xaml.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-07-06</date>

#region

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using io.github.memoryleakx.BoneDockLib.Enums;

#endregion

namespace io.github.memoryleakx.BoneDockLib.UserControls
{
    /// <summary>
    ///     Interaction logic for ContextMenu.xaml
    /// </summary>
    public partial class ContextMenu
    {
        #region Constructors

        public ContextMenu()
        {
            InitializeComponent();
        }

        #endregion

        #region Events and Delegates

        /// <summary>
        ///     Occurs when [launcher action event].
        /// </summary>
        public event ActionEventDelegate ActionEvent;

        /// <param name="source">The source.</param>
        /// <param name="action">The action.</param>
        public delegate void ActionEventDelegate(object source, ContextMenuActionEnum action);

        #endregion

        #region Properties

        public object Source { get; set; }

        #endregion

        #region  Methods

        /// <summary>
        ///     Adds the docklet entries.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="id">The identifier.</param>
        public void AddDockletEntries(string name, Guid? id)
        {
            var item = new MenuItem {Header = name, Tag = id, Foreground = Brushes.Black};
            item.Click += DockletEntry;
            DockletsMenu.Items.Add(item);
        }

        /// <summary>
        ///     Handles the OnClick event of the AddSeparatorMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void AddSeparatorMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (null != Source)
            {
                OnActionEvent(Source, ContextMenuActionEnum.AddSeparator);
            }
        }


        /// <summary>
        ///     Clears the docklet entries.
        /// </summary>
        public void ClearDockletEntries()
        {
            DockletsMenu.Items.Clear();
        }

        /// <summary>
        ///     Docklets the entry.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void DockletEntry(object sender, RoutedEventArgs e)
        {
            OnActionEvent(sender, ContextMenuActionEnum.Docklet);
        }

        /// <summary>
        ///     Handles the OnClick event of the DockSettingsItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void DockSettingsItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (null != Source)
            {
                OnActionEvent(Source, ContextMenuActionEnum.DockSettings);
            }
        }

        private void ExitMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (null != Source)
            {
                OnActionEvent(Source, ContextMenuActionEnum.Exit);
            }
        }

        /// <summary>
        ///     Called when [action event].
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="action">The action.</param>
        protected virtual void OnActionEvent(object source, ContextMenuActionEnum action)
        {
            ActionEvent?.Invoke(source, action);
        }

        /// <summary>
        ///     Removes the add separator menu item.
        /// </summary>
        public void RemoveAddSeparatorMenuItem()
        {
            AddSeparatorMenuItem.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        ///     Removes the docklets menu.
        /// </summary>
        public void RemoveDockletsMenu()
        {
            DockletsMenu.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        ///     Handles the OnClick event of the RemoveMenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void RemoveMenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            if (null != Source)
            {
                OnActionEvent(Source, ContextMenuActionEnum.Remove);
            }
        }

        /// <summary>
        ///     Shows the add separator menu item.
        /// </summary>
        public void ShowAddSeparatorMenuItem()
        {
            AddSeparatorMenuItem.Visibility = Visibility.Visible;
        }

        #endregion
    }
}