﻿// <copyright file="Separator.xaml.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-24</date>

#region

using System.Windows;
using io.github.memoryleakx.BoneDockLib.Enums;
using io.github.memoryleakx.BoneDockLib.Interfaces;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDockLib.UserControls
{
    /// <summary>
    ///     Interaction logic for Separator.xaml
    /// </summary>
    public partial class Separator : IDockUiControl
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Separator" /> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public Separator(DockItem parent)
        {
            InitializeComponent();

            DockItem = parent;
            SeparatorContextMenu.Source = this;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the context menu element that should appear whenever the context menu is requested through user interface (UI)
        ///     from within this element.
        /// </summary>
        public new ContextMenu ContextMenu => SeparatorContextMenu;

        /// <summary>
        ///     Gets the dock item.
        /// </summary>
        /// <value>
        ///     The dock item.
        /// </value>
        public DockItem DockItem { get; }

        /// <summary>
        ///     Gets the UI control.
        /// </summary>
        /// <value>
        ///     The UI control.
        /// </value>
        public FrameworkElement UiControl => this;

        #endregion

        #region  Methods

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        ///     Sets the dock position.
        /// </summary>
        /// <param name="position">The position.</param>
        public void SetDockPosition(PositionEnum position)
        {
        }

        #endregion
    }
}