﻿// <copyright file="IDocklet.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-22</date>

#region

using System;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Interfaces
{
    public interface IDocklet
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        /// <value>
        ///     The description.
        /// </value>
        string Description { get; set; }

        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        Guid? Id { get; set; }

        /// <summary>
        ///     Gets the instance identifier.
        /// </summary>
        /// <value>
        ///     The instance identifier.
        /// </value>
        Guid? InstanceID { get; set; }

        /// <summary>
        ///     Gets or sets the maximum allowed instances.
        /// </summary>
        /// <value>
        ///     The maximum allowed instances.
        ///     -1 = infinity
        /// </value>
        int MaxAllowedInstances { get; }

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        string Name { get; set; }

        /// <summary>
        ///     Gets or sets the version.
        /// </summary>
        /// <value>
        ///     The version.
        /// </value>
        string Version { get; set; }

        #endregion

        #region  Methods

        /// <summary>
        ///     Gets the UI control.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        object GetUiControl(DockItem item);

        #endregion
    }
}