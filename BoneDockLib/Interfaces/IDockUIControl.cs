﻿// <copyright file="IDockUIControl.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-21</date>

#region

using System.Windows;
using io.github.memoryleakx.BoneDockLib.Enums;
using io.github.memoryleakx.BoneDockLib.UserControls;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Interfaces
{
    public interface IDockUiControl
    {
        #region Properties

        /// <summary>
        ///     Gets the context menu.
        /// </summary>
        /// <value>
        ///     The context menu.
        /// </value>
        ContextMenu ContextMenu { get; }

        /// <summary>
        ///     Gets the dock item.
        /// </summary>
        /// <value>
        ///     The dock item.
        /// </value>
        DockItem DockItem { get; }

        /// <summary>
        ///     Gets the UI control.
        /// </summary>
        /// <value>
        ///     The UI control.
        /// </value>
        FrameworkElement UiControl { get; }

        /// <summary>
        /// Sets the dock position.
        /// </summary>
        /// <param name="position">The position.</param>
        void SetDockPosition(PositionEnum position);

        #endregion

        #region  Methods

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        void Dispose();

        #endregion
    }
}