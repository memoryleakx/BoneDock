﻿// <copyright file="IStackDocklet.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-24</date>

#region

using io.github.memoryleakx.BoneDockLib.UserControls;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Interfaces
{
    /// <param name="sender">The sender.</param>
    /// <param name="isMouseOver">if set to <c>true</c> [is mouse over].</param>
    public delegate void MouseOverDelegate(object sender, bool isMouseOver);

    /// <param name="sender">The sender.</param>
    /// <param name="isStaysOpen">if set to <c>true</c> [is stays open].</param>
    public delegate void StayDockOpenDelegate(object sender, bool isStaysOpen);

    /// <param name="sender">The sender.</param>
    /// <param name="items">The items.</param>
    public delegate void LoadedDelegate(object sender, DockItem[] items);

    /// <param name="sender">The sender.</param>
    public delegate void PopupOpensDelegate(object sender);

    public interface IStackDocklet
    {
        #region Events and Delegates

        /// <summary>
        ///     Occurs when [mouse over].
        /// </summary>
        event MouseOverDelegate MouseOver;

        /// <summary>
        ///     Occurs when [stack docklet loaded].
        /// </summary>
        event LoadedDelegate StackDockletLoaded;

        /// <summary>
        ///     Occurs when [stay dock open].
        /// </summary>
        event StayDockOpenDelegate StayDockOpen;

        /// <summary>
        /// Occurs when [popup opens].
        /// </summary>
        event PopupOpensDelegate PopupOpens;

        #endregion

        #region  Methods

        /// <summary>
        ///     Adds the launcher.
        /// </summary>
        /// <param name="launcher">The launcher.</param>
        void AddLauncher(Launcher launcher);

        /// <summary>
        ///     Closes this instance.
        /// </summary>
        void Close();

        /// <summary>
        ///     Loads the settings.
        /// </summary>
        void LoadSettings();

        /// <summary>
        ///     Removes this instance.
        /// </summary>
        void Remove();

        /// <summary>
        ///     Removes the launcher.
        /// </summary>
        /// <param name="launcher">The launcher.</param>
        void RemoveLauncher(Launcher launcher);

        #endregion
    }
}