﻿// <copyright file="Dock.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System;
using System.Xml.Serialization;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Xml
{
    public class Dock
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the dock item.
        /// </summary>
        /// <value>
        ///     The dock item.
        /// </value>
        [XmlArray("DockItems")]
        [XmlArrayItem("Item")]
        public DockItem[] DockItem { get; set; }

        /// <summary>
        ///     Gets or sets the last update.
        /// </summary>
        /// <value>
        ///     The last update.
        /// </value>
        public DateTime LastUpdate { get; set; }

        #endregion
    }
}