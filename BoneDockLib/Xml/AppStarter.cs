﻿// <copyright file="AppStarter.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System.Drawing;
using System.IO;
using System.Xml.Serialization;
using io.github.memoryleakx.BoneDockLib.Helper;
using Image = System.Windows.Controls.Image;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Xml
{
    public class AppStarter
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AppStarter" /> class.
        /// </summary>
        public AppStarter()
        {
            WorkDirectory = string.Empty;
            FileName = string.Empty;
            IconData = string.Empty;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the name of the file.
        /// </summary>
        /// <value>
        ///     The name of the file.
        /// </value>
        public string FileName { get; set; }

        /// <summary>
        ///     Gets the hash identifier.
        /// </summary>
        /// <value>
        ///     The hash identifier.
        /// </value>
        public string HashId { get; set; }

        /// <summary>
        ///     Gets the icon.
        /// </summary>
        /// <value>
        ///     The icon.
        /// </value>
        [XmlIgnore]
        public Image Icon => string.IsNullOrEmpty(IconData) ? new Image() : ImageHelper.GetImageFromString(IconData);

        /// <summary>
        ///     Gets or sets the icon data. (base 64 String)
        /// </summary>
        /// <value>
        ///     The icon data.
        /// </value>
        public string IconData { get; set; }

        /// <summary>
        ///     Gets or sets the work directory.
        /// </summary>
        /// <value>
        ///     The work directory.
        /// </value>
        public string WorkDirectory { get; set; }

        #endregion

        #region  Methods

        /// <summary>
        ///     Sets the executable.
        /// </summary>
        /// <param name="exeFile">The executable file.</param>
        public void SetExecutable(FileInfo exeFile)
        {
            if (".exe" != exeFile.Extension.ToLower())
            {
                return;
            }

            WorkDirectory = exeFile.DirectoryName;
            FileName = exeFile.Name;
            HashId = StringHelper.CalculateMd5Hash(WorkDirectory + FileName);

            Icon ico = ImageHelper.GetIconFromFile(exeFile.FullName);
            if (ico == null)
            {
                return;
            }
            Image icoImage = ImageHelper.GetImageFromBitmap(ico.ToBitmap());
            IconData = ImageHelper.GetStringFromImage(icoImage);
        }

        #endregion
    }
}