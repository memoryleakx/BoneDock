﻿// <copyright file="DockItem.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System;
using io.github.memoryleakx.BoneDockLib.Enums;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Xml
{
    public class DockItem
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the application starter.
        /// </summary>
        /// <value>
        ///     The application starter.
        /// </value>
        public AppStarter AppStarter { get; set; }

        /// <summary>
        ///     Gets or sets the docklet identifier.
        /// </summary>
        /// <value>
        ///     The docklet identifier.
        /// </value>
        public Guid? DockletID { get; set; }

        /// <summary>
        /// Gets the instance identifier.
        /// </summary>
        /// <value>
        /// The instance identifier.
        /// </value>
        public Guid? InstanceID { get; set; }

        /// <summary>
        ///     Gets or sets the position.
        /// </summary>
        /// <value>
        ///     The position.
        /// </value>
        public int Position { get; set; }

        /// <summary>
        ///     Gets or sets the type.
        /// </summary>
        /// <value>
        ///     The type.
        /// </value>
        public DockItemTypeEnum Type { get; set; }

        #endregion
    }
}