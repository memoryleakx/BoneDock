﻿// <copyright file="DockXmlSerializer.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Xml
{
    public class DockXmlSerializer
    {
        #region Fields

        private readonly Encoding m_defaultEncoding;

        private readonly string m_defaultPath;
        private readonly XmlSerializer m_serializerObj;

        #endregion

        #region Static Fields and Constants

        /// <summary>
        ///     The default file name
        /// </summary>
        private const string DefaultFileName = "Dock.xml";

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DockXmlSerializer" /> class.
        /// </summary>
        public DockXmlSerializer()
        {
            m_defaultEncoding = Encoding.UTF8;
            m_serializerObj = new XmlSerializer(typeof(Dock));
            m_defaultPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                            Path.DirectorySeparatorChar + @"BoneDock" + Path.DirectorySeparatorChar;

            if (!Directory.Exists(m_defaultPath))
            {
                Directory.CreateDirectory(m_defaultPath);
            }
        }

        #endregion

        #region  Methods

        /// <summary>
        ///     Load an object from an xml file
        /// </summary>
        /// <returns>
        ///     The object created from the xml file
        /// </returns>
        public Dock Load()
        {
            string fileName = m_defaultPath + DefaultFileName;
            if (!File.Exists(fileName))
            {
                return new Dock();
            }

            try
            {
                using (FileStream stream = File.OpenRead(fileName))
                {
                    return m_serializerObj.Deserialize(stream) as Dock;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return new Dock();
        }

        /// <summary>
        ///     Saves to an xml file
        /// </summary>
        /// <param name="dock">The dock.</param>
        public void Save(Dock dock)
        {
            try
            {
                string fileName = m_defaultPath + DefaultFileName;
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                dock.LastUpdate = DateTime.Now;
                using (var writer = new StreamWriter(m_defaultPath + DefaultFileName, true, m_defaultEncoding))
                {
                    m_serializerObj.Serialize(writer, dock);
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                var freshDock = new Dock {LastUpdate = DateTime.Now};
                Save(freshDock);

                Debug.WriteLine(ex.Message);
            }
        }

        #endregion
    }
}