﻿// <copyright file="ImageHelper.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;
using Image = System.Windows.Controls.Image;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Helper
{
    public class ImageHelper
    {
        #region  Methods

        /// <summary>
        ///     Gets the icon from file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public static Icon GetIconFromFile(string fileName)
        {
            return Icon.ExtractAssociatedIcon(fileName);
        }

        /// <summary>
        ///     Gets the image from bitmap.
        /// </summary>
        /// <param name="bitmap">The bitmap.</param>
        /// <returns></returns>
        public static Image GetImageFromBitmap(Bitmap bitmap)
        {
            var image = new Image();

            using (var memory = new MemoryStream())
            {
                bitmap.MakeTransparent();
                bitmap.Save(memory, ImageFormat.Png);
                memory.Position = 0;
                var bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
                bitmapimage.Freeze();
                image.Source = bitmapimage;
            }
            return image;
        }

        /// <summary>
        ///     Gets the image from string.
        /// </summary>
        /// <param name="base64String">The base64 string.</param>
        /// <returns></returns>
        public static Image GetImageFromString(string base64String)
        {
            byte[] binaryData = Convert.FromBase64String(base64String);

            var bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = new MemoryStream(binaryData);
            bi.EndInit();

            var img = new Image {Source = bi};

            return img;
        }

        /// <summary>
        ///     Gets the string from image.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <returns></returns>
        public static string GetStringFromImage(Image image)
        {
            var bitmapSource = image?.Source as BitmapSource;

            if (bitmapSource == null)
            {
                return string.Empty;
            }

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));

            using (var stream = new MemoryStream())
            {
                encoder.Save(stream);
                return Convert.ToBase64String(stream.ToArray());
            }
        }

        #endregion
    }
}