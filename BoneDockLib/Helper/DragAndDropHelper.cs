﻿// <copyright file="DragAndDropHelper.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-20</date>

#region

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Helper
{
    public class DragAndDropHelper
    {
        #region Static Fields and Constants

        private static Point m_currentMousePosition;
        private static byte m_dragFeedbackCounter;
        private static Point? m_dragStartPoint;

        #endregion

        #region  Methods

        /// <summary>
        ///     Changes the drop item position.
        /// </summary>
        /// <param name="sourcePanel">The source panel.</param>
        /// <param name="data">The data.</param>
        private static void ChangeDropItemPosition(Panel sourcePanel, object data)
        {
            int droptargetIndex = -1;
            Button targetButton = null;

            targetButton = data as Button;
            droptargetIndex = GetDropTargetIndex(sourcePanel, m_currentMousePosition);

            if (-1 == droptargetIndex)
            {
                return;
            }

            if (null == targetButton)
            {
                return;
            }
            DragDropRemoveAndInsert(sourcePanel, targetButton, droptargetIndex);
            Debug.WriteLine("DockItem Position: " + droptargetIndex);
        }

        /// <summary>
        ///     Drags the specified start point.
        /// </summary>
        /// <param name="mouseEventArgs">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        public static void Drag(MouseEventArgs mouseEventArgs)
        {
            if (null == m_dragStartPoint)
            {
                return;
            }
            if (mouseEventArgs.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }

            try
            {
                Point mousePosition = mouseEventArgs.GetPosition(null);
                Vector diff = (Point) m_dragStartPoint - mousePosition;

                object dragData = null;
                DependencyObject control = null;

                if ((mouseEventArgs.LeftButton != MouseButtonState.Pressed ||
                     !(Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance)) &&
                    !(Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
                {
                    return;
                }


                var dockItem = FindAnchestor<Button>((DependencyObject) mouseEventArgs.OriginalSource);
                if (null == dockItem)
                {
                    return;
                }
                dragData = new DataObject("dockitem", dockItem);
                control = dockItem;

                DragDrop.DoDragDrop(control, dragData, DragDropEffects.Move);
            }
            catch (COMException ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        ///     Drags the drop remove and insert.
        /// </summary>
        /// <param name="sourcePanel">The source panel.</param>
        /// <param name="element">The element.</param>
        /// <param name="droptargetIndex">Index of the droptarget.</param>
        private static void DragDropRemoveAndInsert(Panel sourcePanel, UIElement element, int droptargetIndex)
        {
            sourcePanel.Children.Remove(element);
            sourcePanel.Children.Insert(droptargetIndex, element);
            element.ReleaseMouseCapture();
        }

        /// <summary>
        ///     Drags the enter.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="sourcePanel">The source panel.</param>
        /// <param name="dragEventArgs">The <see cref="DragEventArgs" /> instance containing the event data.</param>
        public static void DragEnter(object sender, Panel sourcePanel, DragEventArgs dragEventArgs)
        {
            m_currentMousePosition = dragEventArgs.GetPosition(sourcePanel);
            if (!dragEventArgs.Data.GetDataPresent("dockitem") || sender == dragEventArgs.Source)
            {
                dragEventArgs.Effects = DragDropEffects.None;
            }
        }

        /// <summary>
        ///     Drops the specified drag event arguments.
        /// </summary>
        /// <param name="dragEventArgs">The <see cref="DragEventArgs" /> instance containing the event data.</param>
        public static void Drop(DragEventArgs dragEventArgs)
        {
            if (!dragEventArgs.Data.GetDataPresent("dockitem"))
            {
                return;
            }

            m_dragStartPoint = null;
        }

        /// <summary>
        ///     Drops the files.
        /// </summary>
        /// <param name="filesObj">The files object.</param>
        /// <returns></returns>
        public static AppStarter DropFiles(object filesObj)
        {
            if (null == filesObj || typeof(string[]) != filesObj.GetType())
            {
                return new AppStarter();
            }

            var files = (string[]) filesObj;
            foreach (string file in files)
            {
                if (!File.Exists(file))
                {
                    continue;
                }

                string exeFile = string.Empty;
                if (".lnk" == Path.GetExtension(file))
                {
                    exeFile = FileHelper.GetLnkTarget(file);
                    if (string.IsNullOrEmpty(exeFile))
                    {
                        if (file.Contains("File Explorer.lnk"))
                        {
                            string sys32 = Environment.SystemDirectory;
                            exeFile = sys32 + Path.DirectorySeparatorChar + "explorer.exe";
                        }
                    }
                }

                if (".exe" == Path.GetExtension(file))
                {
                    exeFile = file;
                }

                if (string.IsNullOrEmpty(exeFile))
                {
                    continue;
                }

                var starter = new AppStarter();
                starter.SetExecutable(new FileInfo(exeFile));
                return starter;
            }
            return new AppStarter();
        }

        /// <summary>
        ///     Finds the anchestor.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="current">The current.</param>
        /// <returns></returns>
        internal static T FindAnchestor<T>(DependencyObject current) where T : DependencyObject
        {
            do
            {
                var anchestor = current as T;
                if (null != anchestor)
                {
                    return anchestor;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }

        /// <summary>
        ///     Gets the index of the drop target.
        /// </summary>
        /// <param name="sourcePanel">The source panel.</param>
        /// <param name="currentPoint">The current point.</param>
        /// <returns></returns>
        private static int GetDropTargetIndex(Panel sourcePanel, Point currentPoint)
        {
            if (null == sourcePanel)
            {
                return -1;
            }
            IInputElement element = sourcePanel.InputHitTest(currentPoint);
            var uiElement = element as UIElement;
            if (null == uiElement)
            {
                return -1;
            }

            for (var parent = VisualTreeHelper.GetParent(uiElement) as UIElement;
                parent != null;
                parent = VisualTreeHelper.GetParent(parent) as UIElement)
            {
                if (typeof(Button) == parent.GetType().BaseType)
                {
                    uiElement = parent;
                    break;
                }
            }

            var index = 0;
            foreach (object stackObj in sourcePanel.Children)
            {
                if (stackObj.Equals(uiElement))
                {
                    return index;
                }
                index++;
            }
            return -1;
        }

        /// <summary>
        ///     Gives the feedback.
        /// </summary>
        /// <param name="sourcePanel">The source panel.</param>
        /// <param name="giveFeedbackEventArgs">The <see cref="GiveFeedbackEventArgs" /> instance containing the event data.</param>
        public static void GiveFeedback(Panel sourcePanel, GiveFeedbackEventArgs giveFeedbackEventArgs)
        {
            if (DragDropEffects.Move != giveFeedbackEventArgs.Effects)
            {
                return;
            }

            var dockItem = FindAnchestor<Button>((DependencyObject) giveFeedbackEventArgs.OriginalSource);
            if (null != dockItem)
            {
                if (dockItem.ActualWidth < 50)
                {
                    if (30 == m_dragFeedbackCounter)
                    {
                        m_dragFeedbackCounter = 0;
                        ChangeDropItemPosition(sourcePanel, dockItem);
                    }
                }
                else
                {
                    ChangeDropItemPosition(sourcePanel, dockItem);
                }
            }
            m_dragFeedbackCounter++;
        }

        /// <summary>
        ///     Mouses the left button down.
        /// </summary>
        /// <param name="mouseButtonEventArgs">
        ///     The <see cref="System.Windows.Input.MouseButtonEventArgs" /> instance containing the
        ///     event data.
        /// </param>
        public static void MouseLeftButtonDown(MouseButtonEventArgs mouseButtonEventArgs)
        {
            m_dragFeedbackCounter = 0;
            m_dragStartPoint = mouseButtonEventArgs.GetPosition(null);
        }

        #endregion
    }
}