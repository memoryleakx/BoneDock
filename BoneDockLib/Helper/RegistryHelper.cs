﻿// <copyright file="RegistryHelper.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System.Reflection;
using Microsoft.Win32;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Helper
{
    public static class RegistryHelper
    {
        #region Static Fields and Constants

        /// <summary>
        ///     The application name
        /// </summary>
        private const string AppName = "BoneDock";

        /// <summary>
        ///     The registry key
        /// </summary>
        private static readonly RegistryKey RegistryKey =
            Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        #endregion

        #region  Methods

        /// <summary>
        ///     Automatics the start flag exist.
        /// </summary>
        /// <returns></returns>
        public static bool AutoStartFlagExist()
        {
            object regObj = RegistryKey?.GetValue(AppName);
            return null != regObj;
        }

        /// <summary>
        ///     Removes the automatic start flag.
        /// </summary>
        public static void RemoveAutoStartFlag()
        {
            RegistryKey?.DeleteValue(AppName, false);
        }

        public static void SetAutoStartFlag()
        {
            RegistryKey.SetValue(AppName, Assembly.GetExecutingAssembly().Location);
        }

        #endregion
    }
}