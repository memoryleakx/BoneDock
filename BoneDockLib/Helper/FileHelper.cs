﻿// <copyright file="FileHelper.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-24</date>

#region

using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using IWshRuntimeLibrary;
using File = System.IO.File;

#endregion

namespace io.github.memoryleakx.BoneDockLib.Helper
{
    public class FileHelper
    {
        #region  Methods

        /// <summary>
        ///     Assemblies the unique identifier string.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns></returns>
        public static string AssemblyGuidString(Assembly assembly)
        {
            object[] objects = assembly.GetCustomAttributes(typeof(GuidAttribute), false);
            return objects.Length > 0 ? ((GuidAttribute) objects[0]).Value : string.Empty;
        }

        /// <summary>
        ///     Gets the LNK target.
        /// </summary>
        /// <param name="lnkPath">The LNK path.</param>
        /// <returns></returns>
        public static string GetLnkTarget(string lnkPath)
        {
            var shell = new WshShell(); //Create a new WshShell Interface
            var link = (IWshShortcut) shell.CreateShortcut(lnkPath); //Link the interface to our shortcut

            string exefile = string.Empty;
            if (File.Exists(link.TargetPath))
            {
                exefile = link.TargetPath;
            }
            else
            {
                var workingDir = new DirectoryInfo(link.WorkingDirectory);
                string filename = Path.GetFileName(link.TargetPath);
                string inWorkDir = workingDir.FullName + Path.DirectorySeparatorChar + filename;
                if (File.Exists(inWorkDir))
                {
                    exefile = inWorkDir;
                }
            }
            return exefile;
        }

        /// <summary>
        ///     Determines whether [has read access to folder] [the specified folder path].
        /// </summary>
        /// <param name="folderPath">The folder path.</param>
        /// <returns></returns>
        public static bool HasReadAccessToFolder(string folderPath)
        {
            var di = new DirectoryInfo(folderPath);
            if (!di.Exists)
            {
                return false;
            }
            try
            {
                DirectorySecurity directorySecurity = di.GetAccessControl();
                return true;
            }
            catch (UnauthorizedAccessException uae)
            {
                if (uae.Message.Contains("read-only"))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        ///     Determines whether [has read write access to folder] [the specified folder path].
        /// </summary>
        /// <param name="folderPath">The folder path.</param>
        /// <returns></returns>
        public static bool HasReadWriteAccessToFolder(string folderPath)
        {
            var di = new DirectoryInfo(folderPath);
            if (!di.Exists)
            {
                return false;
            }
            try
            {
                DirectorySecurity directorySecurity = di.GetAccessControl();
                return true;
            }
            catch (UnauthorizedAccessException uae)
            {
                Debug.WriteLine(uae.Message);
                return false;
            }
        }

        #endregion
    }
}