#include <idp.iss> 

[_ISTool]
EnableISX=true


[Setup]

#define MyAppName "BoneDock"
#define MyAppURL  "http://memoryleakx.github.io"
#define MyAppVersion "0.3"
#define MyAppPublisher "memoryleakx"
#define DateTimeString GetDateTimeString('YYYY/mm/dd hh:nn', '-', ':');
#define TouchDateString GetDateTimeString('YYYY/mm/dd', '-', '');
#define TouchTimeString GetDateTimeString('hh:nn', '', ':');
#define ProgYear GetDateTimeString("yyyy", "", "")
#define SetupFolder "D:\Repos\BoneDock\Setup"


AppMutex=Global\{{64B379CA-2197-4642-BE4A-0574B9C45363}-BoneDock.exe
AppID={{64B379CA-2197-4642-BE4A-0574B9C45363}
AppName={#MyAppName}
AppVerName={#MyAppName} {#MyAppVersion}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DefaultGroupName={#MyAppName}
AllowNoIcons=true
OutputDir={#SetupFolder}\Builds\
OutputBaseFilename={#MyAppName} {#MyAppVersion}
InternalCompressLevel=ultra
Compression=lzma
TimeStampsInUTC=true
SolidCompression=true
WizardImageFile={#SetupFolder}\Images\logo_wide_small25.bmp
WizardSmallImageFile={#SetupFolder}\Images\blank.bmp
WizardImageBackColor=clWhite
WizardImageStretch=no
AppCopyright={#MyAppPublisher} {#ProgYear}
VersionInfoVersion={#MyAppVersion}
VersionInfoCompany={#MyAppPublisher}
SetupLogging=true
PrivilegesRequired=admin
DisableStartupPrompt=yes
ShowLanguageDialog=no
ChangesAssociations=true
AllowUNCPath=true
VersionInfoCopyright={#MyAppPublisher} {#ProgYear}
VersionInfoProductName={#MyAppName}
VersionInfoProductVersion={#MyAppVersion}
SetupIconFile={#SetupFolder}\Images\setup.ico
UninstallDisplayIcon={app}\"BoneDock.exe"
TouchDate={#TouchDateString}
TouchTime={#TouchTimeString}

[Languages]
Name: en; MessagesFile: compiler:Default.isl

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Files]
Source: "..\BoneDock\bin\Release\BoneDockLib.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\BoneDock\bin\Release\BoneDock.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Docklets\LauncherStack\bin\Release\LauncherStack.dll"; DestDir: "{commonappdata}\{#MyAppName}\Docklets"; Flags: ignoreversion
Source: "..\Docklets\RecycleBin\bin\Release\RecycleBin.dll"; DestDir: "{commonappdata}\{#MyAppName}\Docklets"; Flags: ignoreversion

[Icons]
Name: {group}\{#MyAppName}; Filename: {app}\{#MyAppName}.exe; IconIndex: 0; Flags: createonlyiffileexists
Name: {group}\{cm:UninstallProgram,{#MyAppName}}; Filename: {uninstallexe}
Name: {commondesktop}\{#MyAppName}; Filename: {app}\{#MyAppName}.exe; Tasks: desktopicon
Name: {userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}; Filename: {app}\{#MyAppName}.exe; Tasks: quicklaunchicon

[Run]
Filename: "{app}\{#MyAppName}.exe"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent


[ThirdParty]
CompileLogMethod=append

[Code]

function Framework45IsNotInstalled(): Boolean;
var
  bSuccess: Boolean;
  regVersion: Cardinal;
begin
  Result := True;

  bSuccess := RegQueryDWordValue(HKLM, 'Software\Microsoft\NET Framework Setup\NDP\v4\Full', 'Release', regVersion);
  if (True = bSuccess) and (regVersion >= 378389) then begin
    Result := False;
  end;
end;

procedure InitializeWizard;
begin
  if Framework45IsNotInstalled() then
  begin
    idpAddFile('http://go.microsoft.com/fwlink/?LinkId=397707', ExpandConstant('{tmp}\NetFrameworkInstaller.exe'));
    idpDownloadAfter(wpReady);
  end;
end;



procedure InstallFramework;
var
  StatusText: string;
  ResultCode: Integer;
begin
  StatusText := WizardForm.StatusLabel.Caption;
  WizardForm.StatusLabel.Caption := 'Installing .NET Framework 4.5.2. This might take a few minutes�';
  WizardForm.ProgressGauge.Style := npbstMarquee;
  try
     if not Exec(ExpandConstant('{tmp}\NetFrameworkInstaller.exe'), '/passive /norestart', '', SW_SHOW, ewWaitUntilTerminated, ResultCode) then
    begin
      MsgBox('.NET installation failed with code: ' + IntToStr(ResultCode) + '.', mbError, MB_OK);
    end;
  finally
    WizardForm.StatusLabel.Caption := StatusText;
    WizardForm.ProgressGauge.Style := npbstNormal;

    DeleteFile(ExpandConstant('{tmp}\NetFrameworkInstaller.exe'));
  end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
  case CurStep of
    ssPostInstall:
      begin
        if Framework45IsNotInstalled() then
        begin
          InstallFramework();
        end;
      end;
  end;
end;