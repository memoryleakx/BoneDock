﻿// <copyright file="DockletXmlSerializer.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-24</date>

#region

using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDock.Docklets.LauncherStack.Xml
{
    internal class DockletXmlSerializer
    {
        #region Fields

        private readonly Encoding m_defaultEncoding;
        private readonly string m_defaultPath;
        private readonly Guid m_instanceGuid;
        private readonly XmlSerializer m_serializerObj;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="DockXmlSerializer" /> class.
        /// </summary>
        /// <param name="instanceGuid">The instance unique identifier.</param>
        internal DockletXmlSerializer(Guid? instanceGuid)
        {
            if (null == instanceGuid)
            {
                return;
            }

            m_instanceGuid = (Guid) instanceGuid;
            m_defaultEncoding = Encoding.UTF8;
            m_serializerObj = new XmlSerializer(typeof(LauncherStack));
            m_defaultPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                            Path.DirectorySeparatorChar + @"BoneDock" + Path.DirectorySeparatorChar;

            if (!Directory.Exists(m_defaultPath))
            {
                Directory.CreateDirectory(m_defaultPath);
            }
            LauncherStack = new LauncherStack();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the launcher stack.
        /// </summary>
        /// <value>
        ///     The launcher stack.
        /// </value>
        internal LauncherStack LauncherStack { get; private set; }

        #endregion

        #region  Methods

        /// <summary>
        ///     Loads the specified position.
        /// </summary>
        internal void Load()
        {
            string fileName = m_defaultPath + m_instanceGuid + ".xml";
            if (!File.Exists(fileName))
            {
                return;
            }

            try
            {
                using (FileStream stream = File.OpenRead(fileName))
                {
                    LauncherStack = m_serializerObj.Deserialize(stream) as LauncherStack;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        ///     Removes the settings file.
        /// </summary>
        internal void RemoveSettingsFile()
        {
            string fileName = m_defaultPath + m_instanceGuid + ".xml";
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }

        /// <summary>
        ///     Saves to an xml file
        /// </summary>
        internal void Save()
        {
            try
            {
                if (null == LauncherStack)
                {
                    return;
                }

                string fileName = m_defaultPath + m_instanceGuid + ".xml";
                if (File.Exists(fileName))
                {
                    File.Delete(fileName);
                }

                LauncherStack.LastUpdate = DateTime.Now;
                using (var writer = new StreamWriter(fileName, true, m_defaultEncoding))
                {
                    m_serializerObj.Serialize(writer, LauncherStack);
                    writer.Flush();
                }
            }
            catch (Exception ex)
            {
                LauncherStack = new LauncherStack {LastUpdate = DateTime.Now};
                Save();

                Debug.WriteLine(ex.Message);
            }
        }

        #endregion
    }
}