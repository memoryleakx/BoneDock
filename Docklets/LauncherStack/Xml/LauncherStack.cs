﻿// <copyright file="LauncherStack.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-22</date>

#region

using System;
using System.Xml.Serialization;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDock.Docklets.LauncherStack.Xml
{
    public class LauncherStack
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the last update.
        /// </summary>
        /// <value>
        ///     The last update.
        /// </value>
        public DateTime LastUpdate { get; set; }

        /// <summary>
        ///     Gets or sets the dock item.
        /// </summary>
        /// <value>
        ///     The dock item.
        /// </value>
        [XmlArray("Launchers")]
        [XmlArrayItem("Launcher")]
        public DockItem[] Launchers { get; set; }

        /// <summary>
        ///     Gets or sets the position.
        /// </summary>
        /// <value>
        ///     The position.
        /// </value>
        public int Position { get; set; }

        #endregion
    }
}