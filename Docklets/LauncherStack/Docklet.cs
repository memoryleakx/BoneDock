﻿// <copyright file="Docklet.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-06-22</date>

#region

using System;
using System.Reflection;
using io.github.memoryleakx.BoneDockLib.Helper;
using io.github.memoryleakx.BoneDockLib.Interfaces;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDock.Docklets.LauncherStack
{
    public class Docklet : IDocklet
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Docklet" /> class.
        /// </summary>
        public Docklet()
        {
            Name = "Launcher Stack";
            Assembly assembly = Assembly.GetAssembly(typeof(Docklet));
            Version = assembly.GetName().Version.ToString();
            Id = Guid.Parse(FileHelper.AssemblyGuidString(assembly));
            Description = "LauncherStack Docklet for BoneDock";
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the description.
        /// </summary>
        /// <value>
        ///     The description.
        /// </value>
        public string Description { get; set; }


        /// <summary>
        ///     Gets or sets the identifier.
        /// </summary>
        /// <value>
        ///     The identifier.
        /// </value>
        public Guid? Id { get; set; }

        /// <summary>
        /// Gets the instance identifier.
        /// </summary>
        /// <value>
        /// The instance identifier.
        /// </value>
        public Guid? InstanceID { get; set; }

        /// <summary>
        /// Gets or sets the maximum allowed instances.
        /// </summary>
        /// <value>
        /// The maximum allowed instances.
        /// -1 = infinity
        /// </value>
        public int MaxAllowedInstances => -1;

        /// <summary>
        ///     Gets or sets the name.
        /// </summary>
        /// <value>
        ///     The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        ///     Gets or sets the version.
        /// </summary>
        /// <value>
        ///     The version.
        /// </value>
        public string Version { get; set; }

        #endregion

        #region  Methods

        /// <summary>
        ///     Gets or sets the control.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        /// <value>
        ///     The control.
        /// </value>
        public object GetUiControl(DockItem item)
        {
            return item != null ? new UserControls.LauncherStack(item) : null;
        }

        #endregion
    }
}