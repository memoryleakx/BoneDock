﻿// <copyright file="LauncherStack.xaml.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-07-12</date>

#region

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using io.github.memoryleakx.BoneDock.Docklets.LauncherStack.Xml;
using io.github.memoryleakx.BoneDockLib.Enums;
using io.github.memoryleakx.BoneDockLib.Interfaces;
using io.github.memoryleakx.BoneDockLib.UserControls;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDock.Docklets.LauncherStack.UserControls
{
    /// <summary>
    ///     Interaction logic for LauncherStack.xaml
    /// </summary>
    public partial class LauncherStack : IDockUiControl, IStackDocklet
    {
        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="LauncherStack" /> class.
        /// </summary>
        /// <param name="dockItem">The dock item.</param>
        public LauncherStack(DockItem dockItem)
        {
            Launchers = new ObservableCollection<Launcher>();
            DockItem = dockItem;
            InitializeComponent();
            LauncherStackContextMenu.Source = this;
            DockletXmlSerializer = new DockletXmlSerializer(dockItem.InstanceID)
            {
                LauncherStack = {Position = DockItem.Position}
            };

            //Popup Placement (workaraund)
            Popup1.Placement = PlacementMode.Mouse;
        }

        #endregion

        #region Events and Delegates

        /// <summary>
        ///     Occurs when [action event].
        /// </summary>
        public event ContextMenu.ActionEventDelegate ActionEvent
        {
            add { LauncherStackContextMenu.ActionEvent += value; }
            remove { LauncherStackContextMenu.ActionEvent -= value; }
        }

        /// <summary>
        ///     Occurs when [mouse over].
        /// </summary>
        public event MouseOverDelegate MouseOver;

        /// <summary>
        ///     Occurs when [popup opens].
        /// </summary>
        public event PopupOpensDelegate PopupOpens;

        /// <summary>
        ///     Occurs when [stack docklet loaded].
        /// </summary>
        public event LoadedDelegate StackDockletLoaded;

        /// <summary>
        ///     Occurs when [stay dock open].
        /// </summary>
        public event StayDockOpenDelegate StayDockOpen;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the context menu element that should appear whenever the context menu is requested through user interface (UI)
        ///     from within this element.
        /// </summary>
        public new ContextMenu ContextMenu => LauncherStackContextMenu;

        /// <summary>
        ///     Gets the dock item.
        /// </summary>
        /// <value>
        ///     The dock item.
        /// </value>
        public DockItem DockItem { get; }

        /// <summary>
        ///     Gets the docklet XML serializer.
        /// </summary>
        /// <value>
        ///     The docklet XML serializer.
        /// </value>
        private DockletXmlSerializer DockletXmlSerializer { get; }

        /// <summary>
        ///     Gets or sets the launchers.
        /// </summary>
        /// <value>
        ///     The launchers.
        /// </value>
        public ObservableCollection<Launcher> Launchers { get; private set; }

        /// <summary>
        ///     Gets the UI control.
        /// </summary>
        /// <value>
        ///     The UI control.
        /// </value>
        public FrameworkElement UiControl => this;

        #endregion

        #region  Methods

        /// <summary>
        ///     Adds the launcher.
        /// </summary>
        /// <param name="launcher">The launcher.</param>
        public void AddLauncher(Launcher launcher)
        {
            if (null == launcher)
            {
                return;
            }

            Launchers.Add(launcher);

            if (0 < Launchers.Count)
            {
                Icon.Source = Launchers[0].AppStarter?.Icon?.Source;
            }
            UpdateSettings();

            //reset to fix Popup Position (workaraund)
            Popup1.Placement = PlacementMode.Relative;
            Popup1.VerticalOffset = 5;
            Popup1.HorizontalOffset = 50;
        }

        /// <summary>
        ///     Closes the Popup
        /// </summary>
        public void Close()
        {
            Popup1.IsOpen = false;
        }

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            Launchers?.Clear();
            Launchers = null;
        }

        /// <summary>
        ///     Handles the OnClick event of the LauncherStack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void LauncherStack_OnClick(object sender, RoutedEventArgs e)
        {
            Popup1.IsOpen = true != Popup1.IsOpen;

            if (!Popup1.IsOpen)
            {
                return;
            }
            OnPopupOpens();
        }

        /// <summary>
        ///     Handles the OnDragEnter event of the LauncherStack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DragEventArgs" /> instance containing the event data.</param>
        private void LauncherStack_OnDragEnter(object sender, DragEventArgs e)
        {
            OnMouseOver(true);
        }

        /// <summary>
        ///     Handles the OnMouseEnter event of the LauncherStack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        private void LauncherStack_OnMouseEnter(object sender, MouseEventArgs e)
        {
            OnMouseOver(true);
        }


        /// <summary>
        ///     Handles the OnMouseLeave event of the LauncherStack control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        private void LauncherStack_OnMouseLeave(object sender, MouseEventArgs e)
        {
            OnMouseOver(false);
        }

        /// <summary>
        ///     Loads the settings.
        /// </summary>
        public void LoadSettings()
        {
            DockletXmlSerializer?.Load();
            OnStackDockletLoaded(DockletXmlSerializer?.LauncherStack?.Launchers);
        }

        /// <summary>
        ///     Called when [mouse over].
        /// </summary>
        /// <param name="isMouseOver">if set to <c>true</c> [is mouse over].</param>
        protected virtual void OnMouseOver(bool isMouseOver)
        {
            MouseOver?.Invoke(this, isMouseOver);
        }

        /// <summary>
        ///     Called when [popup opens].
        /// </summary>
        protected virtual void OnPopupOpens()
        {
            PopupOpens?.Invoke(this);
        }

        /// <summary>
        ///     Called when [stack docklet loaded].
        /// </summary>
        /// <param name="items">The items.</param>
        protected virtual void OnStackDockletLoaded(DockItem[] items)
        {
            StackDockletLoaded?.Invoke(this, items);
        }

        /// <summary>
        ///     Called when [stay dock open].
        /// </summary>
        /// <param name="isStaysOpen">if set to <c>true</c> [is stays open].</param>
        protected virtual void OnStayDockOpen(bool isStaysOpen)
        {
            StayDockOpen?.Invoke(this, isStaysOpen);
        }

        /// <summary>
        ///     Handles the OnClosed event of the Popup1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void Popup1_OnClosed(object sender, EventArgs e)
        {
            OnStayDockOpen(false);
        }

        /// <summary>
        ///     Handles the OnOpened event of the Popup1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void Popup1_OnOpened(object sender, EventArgs e)
        {
            OnStayDockOpen(true);
        }

        /// <summary>
        ///     Removes this instance.
        /// </summary>
        public void Remove()
        {
            DockletXmlSerializer?.RemoveSettingsFile();
            Dispose();
        }

        /// <summary>
        ///     Removes the launcher.
        /// </summary>
        /// <param name="launcher">The launcher.</param>
        public void RemoveLauncher(Launcher launcher)
        {
            if (null == launcher)
            {
                return;
            }
            Launchers.Remove(launcher);

            Icon.Source = 0 < Launchers.Count ? Launchers[0].AppStarter?.Icon?.Source : null;
            UpdateSettings();
            Popup1.IsOpen = false;
        }

        /// <summary>
        ///     Sets the dock position.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">null</exception>
        public void SetDockPosition(PositionEnum position)
        {
            switch (position)
            {
                case PositionEnum.Bottom:
                    ArrowPath.VerticalAlignment = VerticalAlignment.Top;
                    ArrowPath.Data = Geometry.Parse("M 0 6 L 12 6 L 6 0 Z");
                    Popup1.PlacementRectangle = new Rect(-80, -90, 0, 0);
                    Popup1.PopupAnimation = PopupAnimation.Fade;
                    break;

                case PositionEnum.Top:
                    ArrowPath.VerticalAlignment = VerticalAlignment.Bottom;
                    ArrowPath.Data = Geometry.Parse("M 0 0 L 6 6 L 12 0 Z");
                    Popup1.PlacementRectangle = new Rect(-80, 50, 0, 0);
                    Popup1.PopupAnimation = PopupAnimation.Slide;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(position), position, null);
            }
        }


        /// <summary>
        ///     Updates the settings.
        /// </summary>
        private void UpdateSettings()
        {
            var items = new DockItem[Launchers.Count];
            for (var index = 0; index < Launchers.Count; index++)
            {
                Launcher launcher = Launchers[index];
                var item = new DockItem {AppStarter = launcher.AppStarter, Type = DockItemTypeEnum.AppStarter};
                items[index] = item;
            }
            DockletXmlSerializer.LauncherStack.Launchers = items;
            DockletXmlSerializer.Save();
        }

        #endregion
    }
}