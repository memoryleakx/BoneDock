﻿// <copyright file="RecycleBin.xaml.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-07-07</date>

#region

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using io.github.memoryleakx.BoneDock.Docklets.RecycleBin.Class;
using io.github.memoryleakx.BoneDockLib.Enums;
using io.github.memoryleakx.BoneDockLib.Interfaces;
using io.github.memoryleakx.BoneDockLib.UserControls;
using io.github.memoryleakx.BoneDockLib.Xml;

#endregion

namespace io.github.memoryleakx.BoneDock.Docklets.RecycleBin.UserControls
{
    /// <summary>
    ///     Interaction logic for RecycleBin.xaml
    /// </summary>
    public partial class RecycleBin : IDockUiControl
    {
        #region Fields

        private readonly CancellationToken m_cancellationToken;
        private readonly CancellationTokenSource m_cancellationTokenSource;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="RecycleBin" /> class.
        /// </summary>
        /// <param name="dockItem">The dock item.</param>
        public RecycleBin(DockItem dockItem)
        {
            m_cancellationTokenSource = new CancellationTokenSource();
            m_cancellationToken = m_cancellationTokenSource.Token;

            DockItem = dockItem;
            InitializeComponent();
            RecycleBinContextMenu.Source = this;

            Polling();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the context menu element that should appear whenever the context menu is requested through user
        ///     interface (UI) from within this element.
        /// </summary>
        public new ContextMenu ContextMenu => RecycleBinContextMenu;

        /// <summary>
        ///     Gets the dock item.
        /// </summary>
        /// <value>
        ///     The dock item.
        /// </value>
        public DockItem DockItem { get; }

        /// <summary>
        ///     Gets the UI control.
        /// </summary>
        /// <value>
        ///     The UI control.
        /// </value>
        public FrameworkElement UiControl => this;

        #endregion

        #region  Methods

        /// <summary>
        ///     Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public void Dispose()
        {
            m_cancellationTokenSource?.Cancel();
        }

        /// <summary>
        ///     Gets the size of the recycle bin.
        /// </summary>
        /// <returns></returns>
        public static int GetRecycleBinSize()
        {
            var sqrbi = new WinApi.SHQUERYRBINFO {cbSize = Marshal.SizeOf(typeof(WinApi.SHQUERYRBINFO))};
            WinApi.SHQueryRecycleBin(string.Empty, ref sqrbi);
            return (int) sqrbi.i64NumItems;
        }


        /// <summary>
        ///     Handles the OnClick event of the MenuItem control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            WinApi.SHEmptyRecycleBin(IntPtr.Zero, null, WinApi.RecycleFlag.SHERB_NOSOUND);
        }

        /// <summary>
        ///     Pollings this instance.
        /// </summary>
        private void Polling()
        {
            const int delay = 530;
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    if (m_cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }

                    Action action = SetStatus;
                    Dispatcher.Invoke(DispatcherPriority.Normal, action);

                    Thread.Sleep(delay);
                }
            }, m_cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Default);
        }

        /// <summary>
        ///     Handles the OnClick event of the RecycleBin control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void RecycleBin_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer", "::{645ff040-5081-101b-9f08-00aa002f954e}");
        }

        /// <summary>
        ///     Sets the dock position.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public void SetDockPosition(PositionEnum position)
        {
        }

        /// <summary>
        ///     Sets the status.
        /// </summary>
        private void SetStatus()
        {
            if (0 == GetRecycleBinSize())
            {
                EmptyTrash.Visibility = Visibility.Visible;
                FullTrash.Visibility = Visibility.Collapsed;
            }
            else
            {
                EmptyTrash.Visibility = Visibility.Collapsed;
                FullTrash.Visibility = Visibility.Visible;
            }
        }

        #endregion
    }
}