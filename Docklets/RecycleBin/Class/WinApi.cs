﻿using System;
using System.Runtime.InteropServices;

namespace io.github.memoryleakx.BoneDock.Docklets.RecycleBin.Class
{
    internal class WinApi
    {
        #region  Methods

        [DllImport("Shell32.dll")]
        internal static extern int SHEmptyRecycleBin(IntPtr hwnd, string pszRootPath, RecycleFlag dwFlags);

        /// <summary>
        ///     Shes the query recycle bin.
        /// </summary>
        /// <param name="pszRootPath">The PSZ root path.</param>
        /// <param name="pSHQueryRBInfo">The p sh query rb information.</param>
        /// <returns></returns>
        [DllImport("shell32.dll")]
        internal static extern int SHQueryRecycleBin(string pszRootPath, ref SHQUERYRBINFO pSHQueryRBInfo);

        #endregion

        internal enum RecycleFlag
        {
            SHERB_NOCONFIRMATION = 0x00000001, // No confirmation, when emptying
            SHERB_NOPROGRESSUI = 0x00000001, // No progress tracking window during the emptying of the recycle bin
            SHERB_NOSOUND = 0x00000004 // No sound when the emptying of the recycle bin is complete
        }


        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        internal struct SHQUERYRBINFO
        {
            public int cbSize;
            public long i64Size;
            public long i64NumItems;
        }
    }
}