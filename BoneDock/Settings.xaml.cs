﻿// <copyright file="Settings.xaml.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-05-17</date>

#region

using System.Windows;
using io.github.memoryleakx.BoneDockLib.Enums;
using io.github.memoryleakx.BoneDockLib.Xml.Settings;

#endregion

namespace io.github.memoryleakx.BoneDock
{
    /// <summary>
    ///     Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings
    {
        #region Fields

        private readonly SettingsXmlSerializer m_settingsXmlSerializer;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="Settings" /> class.
        /// </summary>
        public Settings()
        {
            InitializeComponent();
            m_settingsXmlSerializer = new SettingsXmlSerializer();
            SettingsObject = m_settingsXmlSerializer.Load();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the settings.
        /// </summary>
        /// <value>
        ///     The settings.
        /// </value>
        public BoneDockLib.Xml.Settings.Settings SettingsObject { get; }

        #endregion

        #region  Methods

        /// <summary>
        ///     Handles the OnClick event of the CloseBtn control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void CloseBtn_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            switch (SettingsObject.Position)
            {
                case PositionEnum.Top:
                    PositionTop_RBTN.IsChecked = true;
                    break;

                case PositionEnum.Bottom:
                    PositionBottom_RBTN.IsChecked = true;
                    break;
            }

            StartUp_CBox.IsChecked = SettingsObject.AutoStart;
        }

        /// <summary>
        ///     Handles the OnChecked event of the Position_RBTN control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void Position_RBTN_OnChecked(object sender, RoutedEventArgs e)
        {
            if (null != PositionTop_RBTN.IsChecked && (bool) PositionTop_RBTN.IsChecked)
            {
                SettingsObject.Position = PositionEnum.Top;
            }
            else
            {
                SettingsObject.Position = PositionEnum.Bottom;
            }

            m_settingsXmlSerializer.Save(SettingsObject);
        }

        /// <summary>
        ///     Handles the OnChecked event of the StartUp_CBox control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RoutedEventArgs" /> instance containing the event data.</param>
        private void StartUp_CBox_OnChecked(object sender, RoutedEventArgs e)
        {
            if (null == StartUp_CBox.IsChecked)
            {
                return;
            }
            SettingsObject.AutoStart = (bool) StartUp_CBox.IsChecked;

            m_settingsXmlSerializer.Save(SettingsObject);
        }

        #endregion
    }
}