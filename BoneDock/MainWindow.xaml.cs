﻿// <copyright file="MainWindow.xaml.cs">
//    This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
// </copyright>
// <author>memoryleakx</author>
// <date>2016-07-06</date>

#region

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using io.github.memoryleakx.BoneDockLib.Class;
using io.github.memoryleakx.BoneDockLib.Enums;
using io.github.memoryleakx.BoneDockLib.Helper;
using io.github.memoryleakx.BoneDockLib.Interfaces;
using io.github.memoryleakx.BoneDockLib.UserControls;
using io.github.memoryleakx.BoneDockLib.Xml;
using ContextMenu = io.github.memoryleakx.BoneDockLib.UserControls.ContextMenu;
using Dock = io.github.memoryleakx.BoneDockLib.Xml.Dock;
using Separator = io.github.memoryleakx.BoneDockLib.UserControls.Separator;

#endregion

namespace io.github.memoryleakx.BoneDock
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainDock
    {
        #region Fields

        private readonly List<DockItem> m_dockItemTempList;
        private readonly DockXmlSerializer m_dockXmlSerializer;
        private readonly ProcessController m_processController;
        private readonly Settings m_settingsWindow;
        private Dock m_dock;
        private IDocklet[] m_loadedDocklets;
        private PositionEnum m_position;
        private IStackDocklet m_selectedStackDocklet;
        private BoneDockLib.Xml.Settings.Settings m_settings;

        #endregion

        #region Constructors

        public MainDock()
        {
            InitializeComponent();
            DockContextMenu.Source = this;

            m_dockItemTempList = new List<DockItem>();
            m_dockXmlSerializer = new DockXmlSerializer();
            Topmost = true;
            AllowsTransparency = true;
            ShowInTaskbar = false;

            // Load Settings
            m_settingsWindow = new Settings();
            m_settings = m_settingsWindow.SettingsObject;
            m_settingsWindow.Closing += SettingsWindow_Closing;

            m_processController = new ProcessController();
            m_processController.WindowIntersectionEvent += WindowIntersectionEvent;

            var dockletLoader = new DockletLoader();
            dockletLoader.DockletsReceived += DockletsReceived;
            dockletLoader.Load();
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Gets a value indicating whether this instance is context menu is open.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is context menu is open; otherwise, <c>false</c>.
        /// </value>
        public bool IsStayOpen { get; private set; }

        #endregion

        #region  Methods

        /// <summary>
        ///     Launcher_s the action event.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="action">The action.</param>
        private void ActionEvent(object source, ContextMenuActionEnum action)
        {
            if (null == source)
            {
                return;
            }
            Button dockItem = null;
            MenuItem sourceItem = null;

            if (typeof(Button) == source.GetType().BaseType)
            {
                dockItem = (Button) source;
            }

            if (typeof(MenuItem) == source.GetType())
            {
                sourceItem = (MenuItem) source;
            }

            switch (action)
            {
                case ContextMenuActionEnum.AddSeparator:
                    AddSeparator();
                    break;

                case ContextMenuActionEnum.Remove:

                    if (null != m_selectedStackDocklet)
                    {
                        var launcher = dockItem as Launcher;
                        if (null != launcher)
                        {
                            m_selectedStackDocklet?.RemoveLauncher(launcher);
                        }
                    }

                    if (null != dockItem)
                    {
                        var stackDocklet = dockItem as IStackDocklet;
                        if (null != stackDocklet)
                        {
                            stackDocklet.Remove();
                            UnRegisterStackDockletEvents(stackDocklet);
                        }
                        var dockUiControl = (IDockUiControl) dockItem;

                        if (null != dockUiControl.ContextMenu)
                        {
                            dockUiControl.ContextMenu.ActionEvent -= ActionEvent;
                        }
                        DockItem targetItem = dockUiControl.DockItem;
                        if (null != targetItem)
                        {
                            m_dockItemTempList?.Remove(targetItem);
                        }
                        ReorderAppStarterPosition();

                        ObjectStack.Children.Remove(dockItem);
                        dockUiControl.Dispose();
                    }
                    SaveDock();

                    if (m_dockItemTempList != null && !m_dockItemTempList.Any())
                    {
                        DockContextMenu.Visibility = Visibility.Visible;
                    }
                    ShowOrHideContextMenuItems();
                    break;

                case ContextMenuActionEnum.DockSettings:
                    m_settingsWindow.ShowDialog();
                    break;

                case ContextMenuActionEnum.Docklet:
                    if (sourceItem?.Tag is Guid)
                    {
                        var guid = (Guid) sourceItem.Tag;

                        IDocklet sourceDocklet = m_loadedDocklets.FirstOrDefault(p => p.Id == guid);
                        if (null != sourceDocklet)
                        {
                            sourceDocklet.InstanceID = Guid.NewGuid();
                            AddDocklet(sourceDocklet);
                        }
                    }
                    break;

                case ContextMenuActionEnum.Exit:
                    Application.Current.Shutdown();
                    break;
            }
        }


        /// <summary>
        ///     Adds the docklet.
        /// </summary>
        /// <param name="docklet">The docklet.</param>
        /// <param name="load">if set to <c>true</c> [load].</param>
        private void AddDocklet(IDocklet docklet, bool load = false)
        {
            if (null == m_dockItemTempList)
            {
                return;
            }

            if (!AllowAddNewDocklet(docklet))
            {
                return;
            }

            var dItem = new DockItem
            {
                Position = m_dockItemTempList.Count,
                Type = DockItemTypeEnum.Docklet,
                DockletID = docklet.Id,
                InstanceID = docklet.InstanceID
            };

            object uiControl = docklet.GetUiControl(dItem);
            var newUiControl = uiControl as IDockUiControl;
            if (null != newUiControl)
            {
                ((Button) newUiControl).LostMouseCapture += LostMouseCapture;

                if (null != newUiControl.ContextMenu)
                {
                    newUiControl.ContextMenu.ActionEvent += ActionEvent;
                }
                newUiControl.UiControl.Uid = docklet.Id.ToString();

                Action action = delegate
                {
                    if (newUiControl.UiControl != null)
                    {
                        ObjectStack?.Children?.Add(newUiControl.UiControl);
                    }
                };
                Dispatcher.Invoke(DispatcherPriority.Normal, action);
            }

            var newStackDocklet = uiControl as IStackDocklet;
            if (null != newStackDocklet)
            {
                RegisterStackDockletEvents(newStackDocklet);
                newStackDocklet.LoadSettings();
            }
            m_dockItemTempList?.Add(dItem);

            if (!load)
            {
                SaveDock();
            }

            if (1 > ObjectStack?.Children?.Count)
            {
                return;
            }
            UpdateContextMenu(m_loadedDocklets, newUiControl);
            DockContextMenu.Visibility = Visibility.Collapsed;
        }


        /// <summary>
        ///     Adds the docklet menu entries.
        /// </summary>
        /// <param name="contextMenu">The context menu.</param>
        /// <param name="docklets">The docklets.</param>
        private static void AddDockletMenuEntries(ContextMenu contextMenu, IEnumerable<IDocklet> docklets)
        {
            if (null == contextMenu || null == docklets)
            {
                return;
            }

            contextMenu.ClearDockletEntries();
            foreach (IDocklet docklet in docklets)
            {
                contextMenu.AddDockletEntries(docklet.Name, docklet.Id);
            }
        }


        /// <summary>
        ///     Adds the launcher.
        /// </summary>
        /// <param name="starter">The starter.</param>
        /// <param name="firstLoad">if set to <c>true</c> [first load].</param>
        private void AddLauncher(AppStarter starter, bool firstLoad = false)
        {
            if (AppStarterExist(starter))
            {
                return;
            }

            if (null == m_dockItemTempList || string.IsNullOrEmpty(starter.FileName))
            {
                return;
            }

            string tooltipName = StringHelper.UppercaseFirst(Path.GetFileNameWithoutExtension(starter.FileName));

            var dItem = new DockItem {Position = m_dockItemTempList.Count, Type = DockItemTypeEnum.AppStarter};
            var launcher = new Launcher(starter, dItem, m_position) {ToolTip = tooltipName};

            launcher.LostMouseCapture += LostMouseCapture;
            if (null != launcher.ContextMenu)
            {
                launcher.ContextMenu.ActionEvent += ActionEvent;
            }
            dItem.AppStarter = starter;

            if (null == m_selectedStackDocklet)
            {
                Action action = delegate { ObjectStack?.Children?.Add(launcher); };
                Dispatcher.Invoke(DispatcherPriority.Normal, action);
                m_dockItemTempList?.Add(dItem);
            }
            else
            {
                launcher.Margin = new Thickness(5, 2, 5, 2);
                launcher.ContextMenu?.RemoveAddSeparatorMenuItem();
                launcher.ContextMenu?.RemoveDockletsMenu();
                Action action = delegate { m_selectedStackDocklet.AddLauncher(launcher); };
                Dispatcher.Invoke(DispatcherPriority.Normal, action);
            }
            UpdateContextMenu(m_loadedDocklets, launcher);

            if (!firstLoad)
            {
                SaveDock();
            }


            if (1 >= ObjectStack?.Children?.Count)
            {
                return;
            }
            DockContextMenu.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        ///     Adds the separator.
        /// </summary>
        /// <param name="firstLoad">if set to <c>true</c> [first load].</param>
        private void AddSeparator(bool firstLoad = false)
        {
            var dockItem = new DockItem {Position = m_dockItemTempList.Count, Type = DockItemTypeEnum.Separator};
            var sourceSeperator = new Separator(dockItem);
            sourceSeperator.LostMouseCapture += LostMouseCapture;
            if (null != sourceSeperator.ContextMenu)
            {
                sourceSeperator.ContextMenu.ActionEvent += ActionEvent;
            }
            m_dockItemTempList.Add(dockItem);
            ObjectStack.Children.Add(sourceSeperator);

            if (!firstLoad)
            {
                SaveDock();
            }
            DockContextMenu.Visibility = Visibility.Collapsed;
        }


        /// <summary>
        ///     Allows the add new docklet.
        /// </summary>
        /// <param name="docklet">The docklet.</param>
        /// <returns></returns>
        private bool AllowAddNewDocklet(IDocklet docklet)
        {
            var allow = false;
            if (null == m_dockItemTempList || null == docklet)
            {
                return false;
            }
            int existingInstances = m_dockItemTempList.Count(n => n.DockletID == docklet.Id);
            if (docklet.MaxAllowedInstances != -1)
            {
                if (existingInstances < docklet.MaxAllowedInstances)
                {
                    allow = true;
                }
            }
            else
            {
                allow = true;
            }
            return allow;
        }

        /// <summary>
        ///     Launchers the exist.
        /// </summary>
        /// <param name="launcher">The launcher.</param>
        /// <returns></returns>
        private bool AppStarterExist(AppStarter launcher)
        {
            return
                m_dockItemTempList.Where(tmpDockItem => tmpDockItem?.AppStarter != null)
                    .Any(tmpDockItem => tmpDockItem.AppStarter.HashId == launcher.HashId);
        }

        /// <summary>
        ///     Closes all stack docklet popups.
        /// </summary>
        private void CloseAllStackDockletPopups()
        {
            if (null == ObjectStack?.Children)
            {
                return;
            }
            foreach (object child in ObjectStack?.Children)
            {
                var stackitem = child as IStackDocklet;
                if (null == stackitem)
                {
                    continue;
                }
                var item = child as IDockUiControl;
                if (null == item)
                {
                    continue;
                }
                stackitem.Close();
            }
        }

        /// <summary>
        ///     Contexts the menu mouse enter.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.Input.MouseEventArgs" /> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void ContextMenuMouseEnter(object sender, MouseEventArgs e)
        {
            IsStayOpen = true;
        }

        /// <summary>
        ///     Contexts the menu mouse leave.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void ContextMenuMouseLeave(object sender, MouseEventArgs e)
        {
            IsStayOpen = false;
        }

        /// <summary>
        ///     Docklet_s the mouse over.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="isMouseOver">if set to <c>true</c> [is mouse over].</param>
        private void Docklet_MouseOver(object sender, bool isMouseOver)
        {
            if (null == sender)
            {
                return;
            }
            m_selectedStackDocklet = isMouseOver ? sender as IStackDocklet : null;
        }

        /// <summary>
        ///     Dockletses the received.
        /// </summary>
        /// <param name="docklets">The docklets.</param>
        private void DockletsReceived(IDocklet[] docklets)
        {
            m_loadedDocklets = docklets;
            LoadDock();
            UpdateContextMenu(docklets, null);

            LoadSettings();
        }

        /// <summary>
        ///     Loads the dock.
        /// </summary>
        private void LoadDock()
        {
            ObjectStack?.Children?.Clear(); // reset Stack

            if (null == m_dockXmlSerializer)
            {
                return;
            }
            m_dock = m_dockXmlSerializer.Load();

            if (null == m_dock?.DockItem)
            {
                return;
            }

            IOrderedEnumerable<DockItem> sortedList = m_dock.DockItem.OrderBy(i => i.Position);
            foreach (DockItem dockItem in sortedList)
            {
                switch (dockItem?.Type)
                {
                    case DockItemTypeEnum.AppStarter:
                        if (null != dockItem.AppStarter)
                        {
                            AddLauncher(dockItem.AppStarter, true);
                        }
                        break;

                    case DockItemTypeEnum.Separator:
                        AddSeparator(true);
                        break;

                    case DockItemTypeEnum.Docklet:
                        IDocklet docklet = m_loadedDocklets?.FirstOrDefault(p => p.Id == dockItem.DockletID);
                        if (null != docklet)
                        {
                            docklet.InstanceID = dockItem.InstanceID;
                            AddDocklet(docklet, true);
                        }
                        break;
                }
            }
            ShowOrHideContextMenuItems();
        }

        /// <summary>
        ///     Loads the settings.
        /// </summary>
        private void LoadSettings()
        {
            if (null == m_settings)
            {
                return;
            }
            SwitchPosition(m_settings.Position);
            if (m_settings.AutoStart)
            {
                if (!RegistryHelper.AutoStartFlagExist())
                {
                    RegistryHelper.SetAutoStartFlag();
                }
            }
            else
            {
                RegistryHelper.RemoveAutoStartFlag();
            }
        }

        /// <summary>
        ///     Losts the mouse capture.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        private new static void LostMouseCapture(object sender, MouseEventArgs e)
        {
            if (null == sender || typeof(Button) != sender.GetType().BaseType)
            {
                return;
            }
            var button = sender as Button;
            button?.ReleaseMouseCapture();
        }

        /// <summary>
        ///     Moves the into view.
        /// </summary>
        /// <param name="position">The position.</param>
        /// <param name="workArea">The work area.</param>
        public void MoveIntoView(PositionEnum position, Rect workArea)
        {
            const double offset = 3.0;
            switch (position)
            {
                case PositionEnum.Top:
                    Top = 0 - offset;
                    break;

                case PositionEnum.Bottom:
                    Top = workArea.Height - Height + offset;
                    break;
            }
        }


        /// <summary>
        ///     Called when [closed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OnClosed(object sender, EventArgs e)
        {
            m_processController?.Dispose();
        }


        /// <summary>
        ///     Called when [drag enter].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DragEventArgs" /> instance containing the event data.</param>
        private void OnDragEnter(object sender, DragEventArgs e)
        {
            DragAndDropHelper.DragEnter(sender, ObjectStack, e);
        }


        /// <summary>
        ///     Called when [drop].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="DragEventArgs" /> instance containing the event data.</param>
        private void OnDrop(object sender, DragEventArgs e)
        {
            var files = (string[]) e.Data.GetData(DataFormats.FileDrop);
            if (null == files)
            {
                DragAndDropHelper.Drop(e);
                UpdateDropItemPosition();
                SaveDock();
                return;
            }
            AddLauncher(DragAndDropHelper.DropFiles(files));
        }


        /// <summary>
        ///     Called when [give feedback].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="GiveFeedbackEventArgs" /> instance containing the event data.</param>
        private void OnGiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            DragAndDropHelper.GiveFeedback(ObjectStack, e);
        }


        /// <summary>
        ///     Called when [preview mouse left button down].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseButtonEventArgs" /> instance containing the event data.</param>
        private void OnPreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragAndDropHelper.MouseLeftButtonDown(e);
        }


        /// <summary>
        ///     Called when [preview mouse move].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="MouseEventArgs" /> instance containing the event data.</param>
        private void OnPreviewMouseMove(object sender, MouseEventArgs e)
        {
            DragAndDropHelper.Drag(e);
        }

        /// <summary>
        ///     Called when [size changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="SizeChangedEventArgs" /> instance containing the event data.</param>
        private void OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            Rect workArea = SystemParameters.WorkArea;
            Left = (workArea.Width - e.NewSize.Width)/2 + workArea.Left;
            MoveIntoView(m_position, workArea);
        }

        /// <summary>
        ///     Registers the context menu events.
        /// </summary>
        /// <param name="contextMenu">The context menu.</param>
        private void RegisterContextMenuEvents(IInputElement contextMenu)
        {
            if (null == contextMenu)
            {
                return;
            }
            contextMenu.MouseEnter -= ContextMenuMouseEnter;
            contextMenu.MouseLeave -= ContextMenuMouseLeave;

            contextMenu.MouseEnter += ContextMenuMouseEnter;
            contextMenu.MouseLeave += ContextMenuMouseLeave;
        }

        /// <summary>
        ///     Registers the stack docklet events.
        /// </summary>
        /// <param name="stackDocklet">The stack docklet.</param>
        private void RegisterStackDockletEvents(IStackDocklet stackDocklet)
        {
            if (null == stackDocklet)
            {
                return;
            }
            stackDocklet.MouseOver += Docklet_MouseOver;
            stackDocklet.StayDockOpen += StayDockOpen;
            stackDocklet.StackDockletLoaded += StackDockletLoaded;
            stackDocklet.PopupOpens += StackDocklet_PopupOpens;
        }

        /// <summary>
        ///     Reorders the application starter position.
        /// </summary>
        private void ReorderAppStarterPosition()
        {
            IOrderedEnumerable<DockItem> sortedList = m_dockItemTempList.OrderBy(p => p.Position);
            var pos = 0;
            var tmplist = new List<DockItem>();
            foreach (DockItem dockItem in sortedList)
            {
                if (null == dockItem)
                {
                    continue;
                }
                dockItem.Position = pos;
                pos++;
                tmplist.Add(dockItem);
            }
            m_dockItemTempList.Clear();
            m_dockItemTempList.AddRange(tmplist);
            tmplist.Clear();
        }

        /// <summary>
        ///     Saves the dock.
        /// </summary>
        private void SaveDock()
        {
            if (null != m_dock)
            {
                m_dock.DockItem = m_dockItemTempList.ToArray();
            }
            else
            {
                m_dock = new Dock {DockItem = m_dockItemTempList.ToArray()};
            }
            m_dockXmlSerializer?.Save(m_dock);
        }

        /// <summary>
        ///     Sets the dock position to UI controls.
        /// </summary>
        /// <param name="position">The position.</param>
        private void SetDockPositionToUiControls(PositionEnum position)
        {
            if (null == ObjectStack?.Children)
            {
                return;
            }
            foreach (object child in ObjectStack?.Children)
            {
                var item = child as IDockUiControl;
                item?.SetDockPosition(position);
            }
        }

        /// <summary>
        ///     Handles the Closing event of the SettingsWindow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.CancelEventArgs" /> instance containing the event data.</param>
        private void SettingsWindow_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            m_settingsWindow.Visibility = Visibility.Hidden;

            // Refresh Settings
            m_settings = m_settingsWindow.SettingsObject;
            LoadSettings();
        }


        /// <summary>
        ///     Shows the or hide context menu items.
        /// </summary>
        private void ShowOrHideContextMenuItems()
        {
            if (0 == ObjectStack?.Children?.Count)
            {
                DockContextMenu?.RemoveAddSeparatorMenuItem();
            }
            else
            {
                DockContextMenu?.ShowAddSeparatorMenuItem();
            }
        }

        /// <summary>
        ///     if a popup is open the other popups will closed
        /// </summary>
        /// <param name="sender">The sender.</param>
        private void StackDocklet_PopupOpens(object sender)
        {
            if (null == ObjectStack?.Children || null == sender)
            {
                return;
            }
            var senderDockUiControl = sender as IDockUiControl;
            if (null == senderDockUiControl)
            {
                return;
            }

            foreach (object child in ObjectStack?.Children)
            {
                var stackitem = child as IStackDocklet;
                if (null == stackitem)
                {
                    continue;
                }
                var item = child as IDockUiControl;
                if (null == item)
                {
                    continue;
                }

                if (item.DockItem?.InstanceID != senderDockUiControl.DockItem?.InstanceID)
                {
                    stackitem.Close();
                }
            }
        }

        /// <summary>
        ///     Stacks the docklet loaded.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="items">The items.</param>
        private void StackDockletLoaded(object sender, DockItem[] items)
        {
            if (null == items)
            {
                return;
            }

            m_selectedStackDocklet = sender as IStackDocklet;
            if (null != m_selectedStackDocklet)
            {
                foreach (DockItem item in items)
                {
                    AddLauncher(item.AppStarter, true);
                }
            }
            m_selectedStackDocklet = null;
        }


        /// <summary>
        ///     Stays the dock open.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="isStaysOpen">if set to <c>true</c> [is stays open].</param>
        private void StayDockOpen(object sender, bool isStaysOpen)
        {
            IsStayOpen = isStaysOpen;
            m_selectedStackDocklet = isStaysOpen ? sender as IStackDocklet : null;

            if (!isStaysOpen)
            {
                Focus();
            }
        }

        /// <summary>
        ///     Switches the position.
        /// </summary>
        /// <param name="position">The position.</param>
        private void SwitchPosition(PositionEnum position)
        {
            if (PositionEnum.Top == position)
            {
                TopPositionBorder.Visibility = Visibility.Visible;
                BottomPositionBorder.Visibility = Visibility.Collapsed;
            }
            else
            {
                TopPositionBorder.Visibility = Visibility.Collapsed;
                BottomPositionBorder.Visibility = Visibility.Visible;
            }

            m_position = position;

            Rect workArea = SystemParameters.WorkArea;
            MoveIntoView(m_position, workArea);

            m_processController?.SetDockPosition(m_position, workArea.Height);
            SetDockPositionToUiControls(position);
        }

        /// <summary>
        ///     Registers the stack docklet events.
        /// </summary>
        /// <param name="stackDocklet">The stack docklet.</param>
        private void UnRegisterStackDockletEvents(IStackDocklet stackDocklet)
        {
            if (null == stackDocklet)
            {
                return;
            }
            stackDocklet.MouseOver -= Docklet_MouseOver;
            stackDocklet.StayDockOpen -= StayDockOpen;
            stackDocklet.StackDockletLoaded -= StackDockletLoaded;
            stackDocklet.PopupOpens -= StackDocklet_PopupOpens;
        }

        /// <summary>
        ///     Updates the context menu.
        /// </summary>
        public void UpdateContextMenu(IDocklet[] docklets, IDockUiControl uiControl)
        {
            foreach (object element in ObjectStack.Children)
            {
                if (null == element)
                {
                    continue;
                }

                var isKnownType = false;
                var contextMenu = new ContextMenu();

                if (typeof(Separator) == element.GetType())
                {
                    var seperator = element as Separator;
                    contextMenu = seperator?.ContextMenu;
                    isKnownType = true;
                }
                if (typeof(Launcher) == element.GetType())
                {
                    var launcher = element as Launcher;
                    contextMenu = launcher?.ContextMenu;
                    isKnownType = true;
                }
                if (!isKnownType)
                {
                    if (null != uiControl?.ContextMenu)
                    {
                        contextMenu = uiControl.ContextMenu;
                    }
                }
                AddDockletMenuEntries(contextMenu, docklets);
                RegisterContextMenuEvents(contextMenu);
            }
            AddDockletMenuEntries(DockContextMenu, docklets);
            RegisterContextMenuEvents(DockContextMenu);
        }

        /// <summary>
        ///     Updates the drop item position.
        /// </summary>
        private void UpdateDropItemPosition()
        {
            if (m_dockItemTempList == null)
            {
                return;
            }
            m_dockItemTempList.Clear();
            for (var index = 0; index < ObjectStack.Children.Count; index++)
            {
                object child = ObjectStack.Children[index];
                var uiControl = child as IDockUiControl;
                if (null == uiControl)
                {
                    continue;
                }
                uiControl.DockItem.Position = index;
                m_dockItemTempList.Add(uiControl.DockItem);
            }
        }

        /// <summary>
        ///     Windows the intersection event.
        /// </summary>
        /// <param name="intersect">if set to <c>true</c> [intersect].</param>
        private void WindowIntersectionEvent(bool intersect)
        {
            if (intersect)
            {
                if (IsStayOpen)
                {
                    return;
                }
                if (null != m_selectedStackDocklet)
                {
                    return;
                }

                CloseAllStackDockletPopups();
                Topmost = false;
                Focusable = false;
                IsHitTestVisible = false;
                Opacity = 0.0;
            }
            else
            {
                Topmost = true;
                Focusable = true;
                IsHitTestVisible = true;
                Opacity = 1.0;
            }
        }

        #endregion
    }
}